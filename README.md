###### Group permissions module

It is an extension of Drupal Group module.
Group permissions module allows overriding permissions per a group

1) drush en group_permissions
2) Update group type settings and enable "Enable group permissions"
3) create a group of group type from the previous step
4) open group/[GROUP_ID]/permissions
5) Click "Add group permissions" and set custom permissions
